# Stage: Build
FROM node:12-alpine AS builder
WORKDIR /usr/src/app
COPY . .
# Build NestJS Application
RUN npm install && npm run build

# Stage: Deploy
FROM node:12-alpine
WORKDIR /root/
COPY package*.json ./
RUN apk --update add --no-cache --virtual .build-deps \
  gcc \
  g++ \
  make \
  python \
  && npm ci --only=production \
  && apk del --no-cache .build-deps

# Bundle app source
COPY --from=builder /usr/src/app/dist .
ENV TYPEORM_ENTITIES **/*.entity.js

CMD [ "node", "main.js" ]