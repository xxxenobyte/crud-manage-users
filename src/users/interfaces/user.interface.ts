export interface User {
  name: string;
  login: string;
  password: string;
  roles: string[];
  organization: string;
}
