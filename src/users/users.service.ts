import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { User as UserInterface } from './interfaces/user.interface';
import { User } from './user.entity';
import { userRepository } from '../app.constants';
import { Repository } from 'typeorm';
import { HelpersService } from '../helpers/helpers.service';
import { RolesService } from '../roles/roles.service';
import { OrganizationService } from '../organization/organization.service';

@Injectable()
export class UsersService {
  constructor(
    @Inject(userRepository)
    private usersRepository: Repository<User>,
    private readonly helpers: HelpersService,
    private readonly rolesService: RolesService,
    private readonly organizationsService: OrganizationService,
  ) {}

  public async create(createUser: UserInterface): Promise<{ id: number }> {
    const isExistUser = await this.usersRepository.findOne({
      login: createUser.login,
    });

    if (isExistUser) {
      throw new HttpException(
        `User with same login: "${createUser.login}" is exist`,
        HttpStatus.BAD_REQUEST,
      );
    }

    const user = new User();
    Object.assign(user, {
      ...createUser,
      password: await this.helpers.getHash(createUser.password),
    });

    const [roles, organization] = await Promise.all([
      this.rolesService.createOrGetEntity(createUser.roles),
      this.organizationsService.createOrGetEntity(createUser.organization),
    ]);

    user.roles = roles;
    user.organization = organization;

    return this.usersRepository.save(user).then(({ id }) => {
      return {
        id,
      };
    });
  }

  public async getOne(id: number): Promise<User> {
    const user = await this.usersRepository.findOne({
      where: { id },
      relations: ['roles'],
    });

    if (!user) {
      throw new HttpException(
        `User with id: ${id} not found`,
        HttpStatus.NOT_FOUND,
      );
    }

    return user;
  }

  public async getAll(): Promise<User[]> {
    return this.usersRepository.find({ relations: ['organization'] });
  }

  public async delete(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }
}
