import {
  IsString,
  ArrayNotEmpty,
  ArrayMinSize,
  ArrayMaxSize,
  ArrayUnique,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({
    description: 'The name of a user',
    type: String,
    minLength: 1,
    maxLength: 50,
  })
  @MinLength(1)
  @MaxLength(50)
  @IsString()
  readonly name: string;

  @ApiProperty({
    description: 'The login of a user. Must be unique.',
    type: String,
    minLength: 1,
    maxLength: 50,
  })
  @MinLength(1)
  @MaxLength(50)
  @IsString()
  readonly login: string;

  @ApiProperty({
    description: 'The password of a user',
    type: String,
    minLength: 1,
  })
  @MinLength(10)
  @IsString()
  readonly password: string;

  @ApiProperty({
    description: 'The organization.',
    type: String,
    minLength: 1,
    maxLength: 50,
  })
  @MinLength(1)
  @MaxLength(50)
  @IsString()
  readonly organization: string;

  @ApiProperty({
    description: 'Roles of user',
    type: [String],
    minLength: 1,
  })
  @ArrayNotEmpty()
  @ArrayUnique()
  @ArrayMinSize(1)
  @ArrayMaxSize(3)
  readonly roles: string[];
}
