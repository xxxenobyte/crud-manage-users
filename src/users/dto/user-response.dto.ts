import { ApiProperty } from '@nestjs/swagger';

interface OrganizationAndRoles {
  readonly id: number;
  readonly name: string;
}

export class UserCreated {
  @ApiProperty({
    description: 'ID of user',
    type: Number,
  })
  readonly id: string;
}

export class Users {
  @ApiProperty({
    description: 'Array of users',
    type: [Object],
  })
  readonly id: number;
  readonly name: string;
  readonly login: string;
  readonly organization: OrganizationAndRoles;
}

export class User {
  @ApiProperty({
    description: 'Info about user',
    type: Object,
  })
  readonly id: number;
  readonly name: string;
  readonly login: string;
  readonly roles: OrganizationAndRoles[];
}
