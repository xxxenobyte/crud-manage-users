import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { userProviders } from './user.provider';
import { DatabaseModule } from '../database/database.module';
import { HelpersModule } from '../helpers/helpers.module';
import { RolesModule } from '../roles/roles.module';
import { OrganizationModule } from '../organization/organization.module';

@Module({
  imports: [DatabaseModule, HelpersModule, RolesModule, OrganizationModule],
  controllers: [UsersController],
  providers: [...userProviders, UsersService],
})
export class UsersModule {}
