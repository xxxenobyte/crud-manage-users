import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  ManyToOne,
} from 'typeorm';
import { Role } from '../roles/role.entity';
import { Organization } from '.././organization/organization.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column({ unique: true, length: 50 })
  login: string;

  @Column({ select: false })
  password: string;

  @ManyToMany(
    type => Role,
    role => role.users,
    { cascade: true },
  )
  @JoinTable()
  roles: Role[];

  @ManyToOne(type => Organization, { cascade: true })
  @JoinTable()
  organization: Organization;
}
