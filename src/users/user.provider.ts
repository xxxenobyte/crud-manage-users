import { Connection } from 'typeorm';
import { User } from './user.entity';
import { userRepository, databaseConnection } from '../app.constants';

export const userProviders = [
  {
    provide: userRepository,
    useFactory: (connection: Connection) => connection.getRepository(User),
    inject: [databaseConnection],
  },
];
