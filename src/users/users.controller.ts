import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';
import { CreateUserDto } from './dto/user-create.dto';
import { UsersService } from './users.service';
import {
  ApiTags,
  ApiCreatedResponse,
  ApiResponse,
  ApiOkResponse,
} from '@nestjs/swagger';
import { UserCreated, Users, User } from './dto/user-response.dto';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}
  @Post()
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: UserCreated,
  })
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get(':id')
  @ApiResponse({
    description: 'Get all users with organization',
    type: Users,
  })
  getOne(@Param('id', new ParseIntPipe()) id: number) {
    return this.usersService.getOne(id);
  }

  @Get()
  @ApiOkResponse({
    description: 'Get all users with organization',
    type: User,
  })
  getAll() {
    return this.usersService.getAll();
  }

  @Delete(':id')
  @ApiOkResponse({
    description: 'Response 200 OK',
  })
  delete(@Param('id') id: string) {
    return this.usersService.delete(id);
  }
}
