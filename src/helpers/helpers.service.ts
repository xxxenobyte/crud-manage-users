import { Injectable, Logger } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class HelpersService {
  private readonly logger = new Logger(HelpersService.name);

  public getHash(password: string): Promise<string> {
    const saltRounds = 10;

    return new Promise((resolve, reject) => {
      bcrypt.genSalt(saltRounds, (err, salt) => {
        if (err) {
          this.logger.error(`Getting error in helpers.getHash.genSalt: `);
          this.logger.error(err);
          reject(err);
        }
        bcrypt.hash(password, salt, (error, hash) => {
          if (error) {
            this.logger.error(`Getting error in helpers.getHash.hash: `);
            this.logger.error(err);
            reject(error);
          }
          resolve(hash);
        });
      });
    });
  }
}
