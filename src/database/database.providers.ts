import { createConnection } from 'typeorm';
import { DATABASE_URL } from '../app.config';
import { databaseConnection } from '../app.constants';

export const databaseProviders = [
  {
    provide: databaseConnection,
    useFactory: async () =>
      await createConnection({
        url: DATABASE_URL,
        type: 'postgres',
        database: 'postgres',
        entities: [__dirname + '/../**/*.entity{.ts,.js}'],
        synchronize: true,
        logging: true,
      }),
  },
];
