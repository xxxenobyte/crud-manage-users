import { Module } from '@nestjs/common';
import { RolesService } from './roles.service';
import { roleProviders } from './roles.provider';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [...roleProviders, RolesService],
  exports: [RolesService],
})
export class RolesModule {}
