import { Injectable, Inject } from '@nestjs/common';
import { roleRepository } from '../app.constants';
import { Repository, In } from 'typeorm';
import { Role } from './role.entity';

@Injectable()
export class RolesService {
  constructor(
    @Inject(roleRepository)
    private rolesRepository: Repository<Role>,
  ) {}

  public async createOrGetEntity(names: string[]): Promise<Role[]> {
    const roles = [];
    const notExistRules = [];
    const isExistRoles = await this.rolesRepository.find({
      name: In(names),
    });

    if (!isExistRoles.length) {
      names.forEach(name => {
        const role = new Role();
        role.name = name;
        roles.push(role);
      });
    }

    if (isExistRoles.length) {
      for (const Role of isExistRoles) {
        if (!names.includes(Role.name)) notExistRules.push(Role.name);
        roles.push(Role);
      }
    }

    if (notExistRules.length) {
      notExistRules.forEach(name => {
        const role = new Role();
        role.name = name;
        roles.push(role);
      });
    }

    return roles;
  }
}
