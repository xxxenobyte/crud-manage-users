import { Connection } from 'typeorm';
import { Role } from './role.entity';
import { roleRepository, databaseConnection } from '../app.constants';

export const roleProviders = [
  {
    provide: roleRepository,
    useFactory: (connection: Connection) => connection.getRepository(Role),
    inject: [databaseConnection],
  },
];
