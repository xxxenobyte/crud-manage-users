import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { RolesModule } from './roles/roles.module';
import { OrganizationModule } from './organization/organization.module';
import { HelpersModule } from './helpers/helpers.module';

@Module({
  imports: [UsersModule, RolesModule, OrganizationModule, HelpersModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
