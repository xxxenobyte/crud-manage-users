import { Module } from '@nestjs/common';
import { OrganizationService } from './organization.service';
import { organizationProviders } from './organization.provider';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [...organizationProviders, OrganizationService],
  exports: [OrganizationService],
})
export class OrganizationModule {}
