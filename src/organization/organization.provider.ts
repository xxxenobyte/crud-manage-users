import { Connection } from 'typeorm';
import { Organization } from './organization.entity';
import { organizationRepository, databaseConnection } from '../app.constants';

export const organizationProviders = [
  {
    provide: organizationRepository,
    useFactory: (connection: Connection) =>
      connection.getRepository(Organization),
    inject: [databaseConnection],
  },
];
