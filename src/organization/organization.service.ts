import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { organizationRepository } from '../app.constants';
import { Organization } from './organization.entity';

@Injectable()
export class OrganizationService {
  constructor(
    @Inject(organizationRepository)
    private organizationsRepository: Repository<Organization>,
  ) {}

  public async createOrGetEntity(
    organizationName: string,
  ): Promise<Organization> {
    const isExistOrganization = await this.organizationsRepository.findOne({
      name: organizationName,
    });

    if (!isExistOrganization) {
      const organization = new Organization();
      organization.name = organizationName;
      return organization;
    }

    return isExistOrganization;
  }
}
